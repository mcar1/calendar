#ifndef CALENDARMANAGER_H
#define CALENDARMANAGER_H

#include <QCalendarWidget>
#include <QWidget>
#include <QPen>

class CalendarManager : public QCalendarWidget
{
    Q_OBJECT

public:
    CalendarManager(QWidget* parent = 0);

protected:
    virtual void paintCell(QPainter* painter, const QRect& rect,QDate date) const;
private:
    void getDates();
    QDate start;
    QPen m_outlinePen;
    QList<QDate> m_dates;
public slots:
    void toggleDay();
};

#endif // CALENDARMANAGER_H
