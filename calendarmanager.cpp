#include <QPainter>
#include <QIODevice>
#include <QFile>
#include <QMessageBox>
#include <cmath>
#include "calendarmanager.h"

CalendarManager::CalendarManager(QWidget *parent)
    : QCalendarWidget(parent)
{
    //QColor color(Qt::blue);
    m_outlinePen.setColor(Qt::red);
    //m_transparentBrush.setColor(Qt::transparent);
    //setSelectionMode(NoSelection);
    resize(600,500);
    getDates();
}


void CalendarManager::paintCell(QPainter *painter, const QRect &rect, QDate date) const
{
    QCalendarWidget::paintCell(painter, rect, date);

    if( (((abs(date.daysTo(start))%4)<2) != m_dates.contains(date)) && date>=QDate::currentDate().addDays(-10)) {
        painter->setPen(m_outlinePen);
        //painter->setBrush(QBrush(QColor(Qt::blue)));
        painter->drawRect(rect.adjusted(0, 0, -1, -1));
        //painter->setBackground(m_transparentBrush);
    }
}

void CalendarManager::getDates()
{
    QFile file("data.txt");
    if(!file.open(QIODevice::ReadOnly)) {
        //Error code
        qFatal("Error file");
    }
    QByteArray line = file.readLine();
    start = QDate::fromString(line, "dd.MM.yyyy\r\n" );
    QDate date;
    m_dates.clear();
    while(!file.atEnd()) {
        QByteArray line = file.readLine();
        qDebug()<<line;
        date = QDate::fromString(line, "dd.MM.yyyy\r\n" );
        m_dates.append(date);
    }
    file.close();
}

void CalendarManager::toggleDay()
{
    QDate newDate=selectedDate();
    bool isInList=0;
    QFile file("data.txt");
    if(!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        //Error code
        qFatal("Error file");
    }
    file.write(start.toString("dd.MM.yyyy\r\n").toStdString().c_str());
    for(int i=0; i<m_dates.size(); i++)
    {
        if(m_dates[i]==newDate)
            isInList=1;
        else
            file.write(m_dates[i].toString("dd.MM.yyyy\r\n").toStdString().c_str());

    }
    if(!isInList)
    {
        file.write(newDate.toString("dd.MM.yyyy\r\n").toStdString().c_str());
    }
    file.close();
    getDates();
}
