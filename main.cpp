#include <QtWidgets>
#include <QDebug>
#include "calendarmanager.h"

int main(int argc, char* argv[])
{
    QApplication app(argc,argv);
    QWidget w;
    QVBoxLayout layout;
    CalendarManager* pcm=new CalendarManager;
    layout.addWidget(pcm);
    QPushButton* ppb=new QPushButton("toggle day");
    layout.addWidget(ppb);

//    m.m_dates.append(QDate::currentDate().addDays(1));
//    m.m_dates.append(QDate::currentDate().addDays(2));
//    m.m_dates.append(QDate::currentDate().addDays(4));
//    m.m_dates.append(QDate::currentDate().addDays(5));
//    for (int i = 0; i < m.m_dates.size(); ++i) {
//        qDebug()<<m.m_dates[i]<<"\n";
//    }
//    qDebug()<<m.m_dates.size();
    QObject::connect(ppb,SIGNAL(pressed()),pcm,SLOT(toggleDay()));
    w.setLayout(&layout);
    w.show();

    return app.exec();
}
